import CarService
    from "../../../../src/CarFactory/Business/services/CarService";
import VolkswagenAdapter
    from "../../../../src/CarFactory/Business/adapters/VolkswagenAdapter";
import CarModel from "../../../../src/CarFactory/Business/models/CarModel";
import ToyotaAdapter
    from "../../../../src/CarFactory/Business/adapters/ToyotaAdapter";

describe('Car service tests - volkswagen data', () => {
    let service;

    beforeAll(() => {
        service = new CarService(new VolkswagenAdapter());
    });

    it('it should get a car model instance by its ID', async () => {
        const car = await service.get('c879b76c-4c2c-4ab1-b427-bf4b081d4c63');
        expect(car).toBeInstanceOf(CarModel);
    });

    it('it should get car model instances', async () => {
        const cars = await service.getAll();
        cars.forEach(car => {
            expect(car).toBeInstanceOf(CarModel);
        });
    });
});

describe('Car service tests - toyota data', () => {
    let service;

    beforeAll(() => {
        service = new CarService(new ToyotaAdapter());
    });

    it('it should get a car model instance by its ID', async () => {
        const car = await service.get('dc864a4a-b8c4-4392-9000-1200ee34747f');
        expect(car).toBeInstanceOf(CarModel);
    });

    it('it should get car model instances', async () => {
        const cars = await service.getAll();
        cars.forEach(car => {
            expect(car).toBeInstanceOf(CarModel);
        });
    });
});
