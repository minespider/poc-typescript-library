import ToyotaAdapter from '../../../../src/CarFactory/Business/adapters/ToyotaAdapter';
import CarModel from '../../../../src/CarFactory/Business/models/CarModel';


describe('Toyota manufacturer tests', () => {
    let adapter;

    beforeAll(() => {
        adapter = new ToyotaAdapter();
    });

    it('it should get a car model instance by its ID', async () => {
        const car = await adapter.get('dc864a4a-b8c4-4392-9000-1200ee34747f');
        expect(car).toBeInstanceOf(CarModel);
    });

    it('it should get car model instances', async () => {
        const cars = await adapter.getAll();
        cars.forEach(car => {
            expect(car).toBeInstanceOf(CarModel);
        });
    });
});
