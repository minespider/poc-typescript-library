import CarModel from '../../../../src/CarFactory/Business/models/CarModel';
import VolkswagenAdapter
    from "../../../../src/CarFactory/Business/adapters/VolkswagenAdapter";


describe('Vokswagen manufacturer tests', () => {
    let adapter;

    beforeAll(() => {
        adapter = new VolkswagenAdapter();
    });

    it('it should get a car model instance by its ID', async () => {
        const car = await adapter.get('c879b76c-4c2c-4ab1-b427-bf4b081d4c63');
        expect(car).toBeInstanceOf(CarModel);
    });

    it('it should get car model instances', async () => {
        const cars = await adapter.getAll();
        cars.forEach(car => {
            expect(car).toBeInstanceOf(CarModel);
        });
    });
});
