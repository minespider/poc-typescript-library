import CarFactoryFacade from "../../src/CarFactory/CarFactoryFacade";
import CarModel from "../../src/CarFactory/Business/models/CarModel";

describe('Car factory facade tsts', () => {
    let facade;

    beforeAll(() => {
        facade = new CarFactoryFacade();
    });

    it('it should return Volkswagen cars from service', async () => {
        const service = facade.getVolkswagenService();
        const cars = service.getAll();
        (await cars).forEach(item => {
            expect(item).toBeInstanceOf(CarModel);
            expect(item.manufacturer).toBe('VOLKSWAGEN');
        });
    });

    it('it should return one Volkswagen car from service', async () => {
        const service = facade.getVolkswagenService();
        const car = await service.get('c879b76c-4c2c-4ab1-b427-bf4b081d4c63');
        expect(car).toBeInstanceOf(CarModel);
        expect(car.manufacturer).toBe('VOLKSWAGEN');
    });

    it('it should return Toyota cars from service', async () => {
        const service = facade.getToyotaService();
        const cars = await service.getAll();
        (await cars).forEach(item => {
            expect(item).toBeInstanceOf(CarModel);
            expect(item.manufacturer).toBe('TOYOTA MOTORS');
        });
    });

    it('it should return one Toyota car from service', async () => {
        const service = facade.getToyotaService();
        const car = await service.get('dc864a4a-b8c4-4392-9000-1200ee34747f');
        expect(car).toBeInstanceOf(CarModel);
        expect(car.manufacturer).toBe('TOYOTA MOTORS');
    });
});
