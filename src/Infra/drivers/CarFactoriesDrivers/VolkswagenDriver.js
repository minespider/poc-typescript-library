"use strict";
exports.__esModule = true;
var volkswagen_cars_1 = require("./mocks/volkswagen_cars");
var VolkswagenDriver = /** @class */ (function () {
    function VolkswagenDriver() {
    }
    VolkswagenDriver.prototype.fetchCars = function () {
        return new Promise(function (resolve) { return resolve(volkswagen_cars_1.data); });
    };
    VolkswagenDriver.prototype.fetchCar = function (id) {
        var selected_item = volkswagen_cars_1.data.find(function (item) { return item.id == id; });
        return new Promise(function (resolve) { return resolve(selected_item); });
    };
    return VolkswagenDriver;
}());
exports["default"] = VolkswagenDriver;
//# sourceMappingURL=VolkswagenDriver.js.map