"use strict";
exports.__esModule = true;
exports.data = void 0;
exports.data = [
    {
        'serial_number': 'dc864a4a-b8c4-4392-9000-1200ee34747f',
        'name': 'Corolla',
        'color': 'silver',
        'manufacturer': 'TOYOTA MOTORS',
        'cart_type': 'sedan',
        'engine': {
            'serial_number': '6befbf19-9c25-49c3-916e-eddc659314ac',
            'turbo': true,
            'pistons': 8
        },
        'doors': [
            {
                'serial_number': 'b6b6c69c-1f7f-4e86-a99e-89f2dcad4343',
                'eletric': true,
                'side': 'left',
                'front': false,
                'carType': 'sedan'
            },
            {
                'serial_number': 'e7435e5d-ae3d-4c08-b9b8-e066a890d040',
                'eletric': true,
                'side': 'right',
                'front': false,
                'carType': 'sedan'
            },
            {
                'serial_number': '2b24987a-eb7b-47fa-a002-13c05ccee866',
                'eletric': true,
                'side': 'left',
                'front': true,
                'carType': 'sedan'
            },
            {
                'serial_number': '3753dfe8-3b57-4cd4-8ccf-3a727663822f',
                'eletric': true,
                'side': 'right',
                'front': true,
                'carType': 'sedan'
            },
            {
                'serial_number': '88070fb1-4793-4815-bc4f-5fab106406e4',
                'eletric': true,
                'side': 'right',
                'front': true,
                'carType': 'sedan'
            }
        ],
        'trunkDoor': {
            'serial_number': '4e2910cd-9cc9-4440-a14a-f01cf5393705',
            'eletric': true,
            'carType': 'sedan'
        },
        'multimedia': {
            'serial_number': '1f5c719a-26f6-429e-ad34-bbbe6f5aa35a',
            'blueRaySupport': false,
            'bluetoothSupport': true
        }
    },
    {
        'serial_number': '09ac181f-5ceb-4e34-8f46-f6de1099bc51',
        'name': 'Etios',
        'color': 'white',
        'manufacturer': 'TOYOTA MOTORS',
        'cart_type': 'hatch',
        'engine': {
            'serial_number': 'f640cdb6-d554-45a6-b491-c3cb5667d517',
            'turbo': false,
            'pistons': 6
        },
        'doors': [
            {
                'serial_number': 'b6b6c69c-1f7f-4e86-a99e-89f2dcad4343',
                'eletric': false,
                'side': 'left',
                'front': false,
                'carType': 'hatch'
            },
            {
                'serial_number': 'e7435e5d-ae3d-4c08-b9b8-e066a890d040',
                'eletric': false,
                'side': 'right',
                'front': false,
                'carType': 'hatch'
            },
            {
                'serial_number': '2b24987a-eb7b-47fa-a002-13c05ccee866',
                'eletric': true,
                'side': 'left',
                'front': true,
                'carType': 'hatch'
            },
            {
                'serial_number': '3753dfe8-3b57-4cd4-8ccf-3a727663822f',
                'eletric': true,
                'side': 'right',
                'front': true,
                'carType': 'hatch'
            }
        ],
        'trunkDoor': {
            'serial_number': '4e2910cd-9cc9-4440-a14a-f01cf5393705',
            'eletric': false,
            'carType': 'hatch'
        },
        'multimedia': null
    }
];
//# sourceMappingURL=toyota_cars.js.map