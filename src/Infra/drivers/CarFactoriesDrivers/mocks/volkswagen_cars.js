"use strict";
exports.__esModule = true;
exports.data = void 0;
exports.data = [
    {
        'id': 'c879b76c-4c2c-4ab1-b427-bf4b081d4c63',
        'name': 'Tiguan',
        'color': 'blue',
        'manufacturer': 'VOLKSWAGEN',
        'cart_type': 'hatch',
        'motor_speficiation': {
            'id': '6befbf19-9c25-49c3-916e-eddc659314ac',
            'turbo': false,
            'pistons': 4
        },
        'frontdoors': [
            {
                'id': 'b6b6c69c-1f7f-4e86-a99e-89f2dcad4343',
                'eletric': true,
                'side': 'left',
                'carType': 'hatch'
            },
            {
                'id': 'e7435e5d-ae3d-4c08-b9b8-e066a890d040',
                'eletric': true,
                'side': 'right',
                'carType': 'hatch'
            }
        ],
        "backdoors": [
            {
                'id': '2b24987a-eb7b-47fa-a002-13c05ccee866',
                'eletric': true,
                'side': 'left',
                'front': true,
                'carType': 'hatch'
            },
            {
                'id': '3753dfe8-3b57-4cd4-8ccf-3a727663822f',
                'eletric': true,
                'side': 'right',
                'front': true,
                'carType': 'hatch'
            }
        ],
        "trunk_specifications": {
            'id': '88070fb1-4793-4815-bc4f-5fab106406e4',
            'eletric': true,
            'side': 'back',
            'carType': 'hatch'
        },
        'multimedia_kit': {
            'id': '1f5c719a-26f6-429e-ad34-bbbe6f5aa35a',
            'addons': ['bluetooth', 'blue ray']
        }
    },
    {
        'id': 'ec587dfe-f3d3-4b38-a258-bf9e06c16989',
        'name': 'Passat',
        'color': 'black',
        'manufacturer': 'VOLKSWAGEN',
        'cart_type': 'sedan',
        'motor_speficiation': {
            'id': '6befbf19-9c25-49c3-916e-eddc659314ac',
            'turbo': false,
            'pistons': 4
        },
        'frontdoors': [
            {
                'id': 'b6b6c69c-1f7f-4e86-a99e-89f2dcad4343',
                'eletric': true,
                'side': 'left',
                'carType': 'sedan'
            },
            {
                'id': 'e7435e5d-ae3d-4c08-b9b8-e066a890d040',
                'eletric': true,
                'side': 'right',
                'carType': 'sedan'
            }
        ],
        "backdoors": [
            {
                'id': '2b24987a-eb7b-47fa-a002-13c05ccee866',
                'eletric': true,
                'side': 'left',
                'front': true,
                'carType': 'sedan'
            },
            {
                'id': '3753dfe8-3b57-4cd4-8ccf-3a727663822f',
                'eletric': true,
                'side': 'right',
                'front': true,
                'carType': 'sedan'
            }
        ],
        "trunk_specifications": {
            'id': '88070fb1-4793-4815-bc4f-5fab106406e4',
            'eletric': true,
            'side': 'back',
            'carType': 'hatch'
        },
        'multimedia_kit': {
            'id': '1f5c719a-26f6-429e-ad34-bbbe6f5aa35a',
            'addons': ['bluetooth', 'blue ray', 'wifi']
        }
    }
];
//# sourceMappingURL=volkswagen_cars.js.map