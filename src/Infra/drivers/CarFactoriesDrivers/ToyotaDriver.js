"use strict";
exports.__esModule = true;
var toyota_cars_1 = require("./mocks/toyota_cars");
var VolkswagenDriver = /** @class */ (function () {
    function VolkswagenDriver() {
    }
    VolkswagenDriver.prototype.fetchCars = function () {
        return new Promise(function (resolve) { return resolve(toyota_cars_1.data); });
    };
    VolkswagenDriver.prototype.fetchCar = function (id) {
        var selected_items = toyota_cars_1.data.filter(function (item) { return item.serial_number == id; });
        var selected_item = selected_items.length ? selected_items[0] : null;
        return new Promise(function (resolve) { return resolve(selected_item); });
    };
    return VolkswagenDriver;
}());
exports["default"] = VolkswagenDriver;
//# sourceMappingURL=ToyotaDriver.js.map