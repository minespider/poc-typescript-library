import CarFactoryDriverInterface from '../CarFactoryDriverInterface';
import {data} from "./mocks/toyota_cars";


export default class VolkswagenDriver implements CarFactoryDriverInterface {
    fetchCars(): Promise<any[]> {
        return new Promise((resolve) => resolve(data));
    }

    fetchCar(id: string): Promise<any> {
        const selected_items = data.filter(item => item.serial_number == id);
        const selected_item = selected_items.length ? selected_items[0] : null;
        return new Promise(resolve => resolve(selected_item));
    }
}
