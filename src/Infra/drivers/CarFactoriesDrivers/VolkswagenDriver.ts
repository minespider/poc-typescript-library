import CarFactoryDriverInterface from '../CarFactoryDriverInterface';
import {data} from "./mocks/volkswagen_cars";



export default class VolkswagenDriver implements CarFactoryDriverInterface {
    fetchCars(): Promise<any[]> {
        return new Promise((resolve) => resolve(data));
    }

    fetchCar(id: string): Promise<any> {
        const selected_item = data.find(item => item.id == id);
        return new Promise(resolve => resolve(selected_item));
    }
}
