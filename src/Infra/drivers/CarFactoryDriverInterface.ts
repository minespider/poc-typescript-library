export default interface CarFactoryDriverInterface {
    fetchCars(): Promise<Array<any>>;
    fetchCar(id: string): Promise<any>;
  }