import VolkswagenAdapter from "./Business/adapters/VolkswagenAdapter";
import CarService from "./Business/services/CarService";
import ToyotaAdapter from "./Business/adapters/ToyotaAdapter";

export default class CarFactoryFacade {
    getVolkswagenService(): CarService {
        return new CarService(new VolkswagenAdapter())
    }

    getToyotaService(): CarService {
        return new CarService(new ToyotaAdapter())
    }
}