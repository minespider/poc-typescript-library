"use strict";
exports.__esModule = true;
var CarModel = /** @class */ (function () {
    function CarModel(id, name, color, manufacturer, carType, engine, frontDoorLeft, frontDoorRight, backDoorLeft, backDoorRight, trunkDoor, multimedia) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.manufacturer = manufacturer;
        this.carType = carType;
        this.engine = engine;
        this.frontDoorLeft = frontDoorLeft;
        this.frontDoorRight = frontDoorRight;
        this.backDoorLeft = backDoorLeft;
        this.backDoorRight = backDoorRight;
        this.trunkDoor = trunkDoor;
        this.multimedia = multimedia;
    }
    return CarModel;
}());
exports["default"] = CarModel;
//# sourceMappingURL=CarModel.js.map