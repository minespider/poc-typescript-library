import CarDTO from "../../DTO/CarDTO";
import EngineDTO from "../../DTO/EngineDTO";
import DoorDTO from "../../DTO/DoorDTO";
import TrunkDoorDTO from "../../DTO/TrunkDoorDTO";
import MultimediaDTO from "../../DTO/MultimediaKitDTO";
import CarTypeDTO from "../../DTO/CarTypeDTO";

export default class CarModel implements CarDTO {
    public constructor(
        public id: string,
        public name: string,
        public color: string,
        public manufacturer: string,
        public carType: CarTypeDTO,
        public engine: EngineDTO,
        public frontDoorLeft?: DoorDTO,
        public frontDoorRight?: DoorDTO,
        public backDoorLeft?: DoorDTO,
        public backDoorRight?: DoorDTO,
        public trunkDoor?: TrunkDoorDTO,
        public multimedia?: MultimediaDTO) { }
}