import CarModel from "../models/CarModel";
import CarDTO from "../../DTO/CarDTO";
import VolkswagenDriver
    from "../../../Infra/drivers/CarFactoriesDrivers/VolkswagenDriver";
import ManufacturerAdapter from "./ManufacturerAdapter";


export default class VolkswagenAdapter extends ManufacturerAdapter {
    constructor() {
        super(new VolkswagenDriver());
    }

    createModel(item: any): CarDTO {
        const carType = {
            id: 0,
            name: item.cart_type
        };

        const engine = {
            id: item.motor_speficiation.id,
            turbo: item.motor_speficiation.turbo,
            pistons: item.motor_speficiation.pistons,
            carType: carType
        };

        const trunkDoor: any = {};
        if (item.trunk_specifications) {
            Object.assign(trunkDoor, {
                id: item.trunk_specifications.id,
                eletrict: item.trunk_specifications.eletric,
                carType: carType
            });
        }

        const multimedia: any = {};
        if (item.multimedia_kit) {
            Object.assign(multimedia, {
                id: item.multimedia_kit.serial_number,
                blueRaySupport: 'blue ray' in item.multimedia_kit,
                bluetoothSupport: 'bluetooth' in item.multimedia_kit
            });
        }

        const frontDoorLeft: any = {};
        let frontDoorRight: any = {};

        item.frontdoors.forEach(door_item => {
            const door = {
                id: door_item.id,
                eletric: door_item.eletrict,
                side: door_item.side,
                front: door_item.front,
                carType: carType
            };

            if (door.side == 'left') {
                Object.assign(frontDoorLeft, door);
            } else {
                Object.assign(frontDoorRight, door);
            }
        });

        const backDoorLeft: any = {};
        const backDoorRight: any = {};

        item.backdoors.forEach(door_item => {
            const door = {
                id: door_item.id,
                eletric: door_item.eletrict,
                side: door_item.side,
                front: door_item.front,
                carType: carType
            };

            if (door.side == 'left') {
                Object.assign(backDoorLeft, door);
            } else {
                Object.assign(backDoorRight, door);
            }
        });

        return new CarModel(
            item.id,
            item.name,
            item.color,
            item.manufacturer,
            carType,
            engine,
            frontDoorLeft,
            frontDoorRight,
            backDoorLeft,
            backDoorRight,
            trunkDoor || null,
            multimedia || null
        );
    }
}