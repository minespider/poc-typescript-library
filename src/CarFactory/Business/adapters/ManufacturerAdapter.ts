import CarFactoryDriverInterface
    from "../../../Infra/drivers/CarFactoryDriverInterface";
import CarDTO from "../../DTO/CarDTO";
import ManufacturerAdapterInterface from "./ManufacturerInterface";

export default abstract class ManufacturerAdapter implements ManufacturerAdapterInterface {
    protected driver: CarFactoryDriverInterface;


    protected constructor(driver: CarFactoryDriverInterface) {
        this.driver = driver;
    }

    async getAll(): Promise<CarDTO[]> {
        const collection = await this.driver.fetchCars();

        const items: CarDTO[] = []
        collection.forEach(item => {
            items.push(this.createModel(item));
        });

        return items;
    }

    async get(id: string): Promise<CarDTO|null> {
        const item = await this.driver.fetchCar(id);
        return item ? this.createModel(item) : null;
    }

    abstract createModel(item: any): CarDTO;
}