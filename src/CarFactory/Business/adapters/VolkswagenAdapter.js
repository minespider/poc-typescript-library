"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var CarModel_1 = require("../models/CarModel");
var VolkswagenDriver_1 = require("../../../Infra/drivers/CarFactoriesDrivers/VolkswagenDriver");
var ManufacturerAdapter_1 = require("./ManufacturerAdapter");
var VolkswagenAdapter = /** @class */ (function (_super) {
    __extends(VolkswagenAdapter, _super);
    function VolkswagenAdapter() {
        return _super.call(this, new VolkswagenDriver_1["default"]()) || this;
    }
    VolkswagenAdapter.prototype.createModel = function (item) {
        var carType = {
            id: 0,
            name: item.cart_type
        };
        var engine = {
            id: item.motor_speficiation.id,
            turbo: item.motor_speficiation.turbo,
            pistons: item.motor_speficiation.pistons,
            carType: carType
        };
        var trunkDoor = {};
        if (item.trunk_specifications) {
            Object.assign(trunkDoor, {
                id: item.trunk_specifications.id,
                eletrict: item.trunk_specifications.eletric,
                carType: carType
            });
        }
        var multimedia = {};
        if (item.multimedia_kit) {
            Object.assign(multimedia, {
                id: item.multimedia_kit.serial_number,
                blueRaySupport: 'blue ray' in item.multimedia_kit,
                bluetoothSupport: 'bluetooth' in item.multimedia_kit
            });
        }
        var frontDoorLeft = {};
        var frontDoorRight = {};
        item.frontdoors.forEach(function (door_item) {
            var door = {
                id: door_item.id,
                eletric: door_item.eletrict,
                side: door_item.side,
                front: door_item.front,
                carType: carType
            };
            if (door.side == 'left') {
                Object.assign(frontDoorLeft, door);
            }
            else {
                Object.assign(frontDoorRight, door);
            }
        });
        var backDoorLeft = {};
        var backDoorRight = {};
        item.backdoors.forEach(function (door_item) {
            var door = {
                id: door_item.id,
                eletric: door_item.eletrict,
                side: door_item.side,
                front: door_item.front,
                carType: carType
            };
            if (door.side == 'left') {
                Object.assign(backDoorLeft, door);
            }
            else {
                Object.assign(backDoorRight, door);
            }
        });
        return new CarModel_1["default"](item.id, item.name, item.color, item.manufacturer, carType, engine, frontDoorLeft, frontDoorRight, backDoorLeft, backDoorRight, trunkDoor || null, multimedia || null);
    };
    return VolkswagenAdapter;
}(ManufacturerAdapter_1["default"]));
exports["default"] = VolkswagenAdapter;
//# sourceMappingURL=VolkswagenAdapter.js.map