import ToyotaDriver
    from "../../../Infra/drivers/CarFactoriesDrivers/ToyotaDriver";
import CarModel from "../models/CarModel";
import CarDTO from "../../DTO/CarDTO";
import ManufacturerAdapter from "./ManufacturerAdapter";

export default class ToyotaAdapter extends ManufacturerAdapter {
    constructor() {
        super(new ToyotaDriver());
    }

    createModel(item: any): CarDTO {
        const carType = {
            id: 0,
            name: item.cart_type
        };

        const engine = {
            id: item.engine.serial_number,
            turbo: item.engine.turbo,
            pistons: item.engine.pistons,
            carType: carType
        };

        const trunkDoor: any = {};
        if (item.trunkDoor) {
            Object.assign(trunkDoor, {
                id: item.trunkDoor.serial_number,
                eletrict: item.trunkDoor.eletric === true,
                carType: carType
            });
        }

        const multimedia: any = {};
        if (item.multimedia) {
            Object.assign(multimedia, {
                id: item.multimedia.serial_number,
                blueRaySupport: item.multimedia.blueRaySupport === true,
                bluetoothSupport: item.multimedia.bluetoothSupport === true
            });
        }

        const frontDoorLeft: any = {};
        let frontDoorRight: any = {};
        const backDoorLeft: any = {};
        const backDoorRight: any = {};

        item.doors.forEach((door_item) => {
            const door = {
                id: door_item.serial_number,
                eletric: door_item.eletrict,
                side: door_item.side,
                front: door_item.front,
                carType: carType
            };
            if (door.front == true) {
                if (door.side == 'left') {
                    Object.assign(frontDoorLeft, door);
                } else if (door.side == 'right') {
                    Object.assign(frontDoorRight, door);
                }
            } else {
                if (door.side == 'left') {
                    Object.assign(backDoorLeft, door);
                } else if (door.side == 'right') {
                    Object.assign(backDoorRight, door);
                }
            }
        });

        return new CarModel(
            item.serial_number,
            item.name,
            item.color,
            item.manufacturer,
            carType,
            engine,
            frontDoorLeft,
            frontDoorRight,
            backDoorLeft,
            backDoorRight,
            trunkDoor || null,
            multimedia || null
        );
    }
}