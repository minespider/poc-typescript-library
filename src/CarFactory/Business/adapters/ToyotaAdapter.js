"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var ToyotaDriver_1 = require("../../../Infra/drivers/CarFactoriesDrivers/ToyotaDriver");
var CarModel_1 = require("../models/CarModel");
var ManufacturerAdapter_1 = require("./ManufacturerAdapter");
var ToyotaAdapter = /** @class */ (function (_super) {
    __extends(ToyotaAdapter, _super);
    function ToyotaAdapter() {
        return _super.call(this, new ToyotaDriver_1["default"]()) || this;
    }
    ToyotaAdapter.prototype.createModel = function (item) {
        var carType = {
            id: 0,
            name: item.cart_type
        };
        var engine = {
            id: item.engine.serial_number,
            turbo: item.engine.turbo,
            pistons: item.engine.pistons,
            carType: carType
        };
        var trunkDoor = {};
        if (item.trunkDoor) {
            Object.assign(trunkDoor, {
                id: item.trunkDoor.serial_number,
                eletrict: item.trunkDoor.eletric === true,
                carType: carType
            });
        }
        var multimedia = {};
        if (item.multimedia) {
            Object.assign(multimedia, {
                id: item.multimedia.serial_number,
                blueRaySupport: item.multimedia.blueRaySupport === true,
                bluetoothSupport: item.multimedia.bluetoothSupport === true
            });
        }
        var frontDoorLeft = {};
        var frontDoorRight = {};
        var backDoorLeft = {};
        var backDoorRight = {};
        item.doors.forEach(function (door_item) {
            var door = {
                id: door_item.serial_number,
                eletric: door_item.eletrict,
                side: door_item.side,
                front: door_item.front,
                carType: carType
            };
            if (door.front == true) {
                if (door.side == 'left') {
                    Object.assign(frontDoorLeft, door);
                }
                else if (door.side == 'right') {
                    Object.assign(frontDoorRight, door);
                }
            }
            else {
                if (door.side == 'left') {
                    Object.assign(backDoorLeft, door);
                }
                else if (door.side == 'right') {
                    Object.assign(backDoorRight, door);
                }
            }
        });
        return new CarModel_1["default"](item.serial_number, item.name, item.color, item.manufacturer, carType, engine, frontDoorLeft, frontDoorRight, backDoorLeft, backDoorRight, trunkDoor || null, multimedia || null);
    };
    return ToyotaAdapter;
}(ManufacturerAdapter_1["default"]));
exports["default"] = ToyotaAdapter;
//# sourceMappingURL=ToyotaAdapter.js.map