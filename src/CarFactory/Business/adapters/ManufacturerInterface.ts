import CarDTO from "../../DTO/CarDTO";

export default interface ManufacturerAdapterInterface {
    getAll(): Promise<CarDTO[]>;
    get(id: string): Promise<CarDTO|null|undefined>;
  }