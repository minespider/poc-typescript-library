import ManufacturerAdapterInterface from "../adapters/ManufacturerInterface";
import CarDTO from "../../DTO/CarDTO";

export default class CarService {
    constructor(protected adapter: ManufacturerAdapterInterface) {}

    async getAll(): Promise<CarDTO[]> {
        return await this.adapter.getAll();
    }

    async get(id: string): Promise<CarDTO|null|undefined> {
        return await this.adapter.get(id);
    }
}