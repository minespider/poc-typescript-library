"use strict";
exports.__esModule = true;
var VolkswagenAdapter_1 = require("./Business/adapters/VolkswagenAdapter");
var CarService_1 = require("./Business/services/CarService");
var ToyotaAdapter_1 = require("./Business/adapters/ToyotaAdapter");
var CarFactoryFacade = /** @class */ (function () {
    function CarFactoryFacade() {
    }
    CarFactoryFacade.prototype.getVolkswagenService = function () {
        return new CarService_1["default"](new VolkswagenAdapter_1["default"]());
    };
    CarFactoryFacade.prototype.getToyotaService = function () {
        return new CarService_1["default"](new ToyotaAdapter_1["default"]());
    };
    return CarFactoryFacade;
}());
exports["default"] = CarFactoryFacade;
//# sourceMappingURL=CarFactoryFacade.js.map