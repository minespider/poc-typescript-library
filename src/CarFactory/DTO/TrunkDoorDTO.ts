import CarTypeDTO from './CarTypeDTO';

export default interface TrunkDoorDTO {
  id: number;
  eletric: boolean;
  carType: CarTypeDTO;
}
