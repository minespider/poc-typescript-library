export default interface CarTypeDTO {
  id: number;
  name: string;
}
