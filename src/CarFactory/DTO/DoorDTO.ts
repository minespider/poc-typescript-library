import CarTypeDTO from './CarTypeDTO';

export default interface DoorDTO {
  id: number;
  eletric: boolean;
  side: string;
  front: boolean;
  carType: CarTypeDTO;
}
