export default interface MultimediaDTO {
  id: number;
  blueRaySupport: boolean;
  bluetoothSupport: boolean;
}
