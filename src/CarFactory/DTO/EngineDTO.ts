import CarTypeDTO from './CarTypeDTO';

export default interface EngineDTO {
  id: number;
  turbo: boolean;
  pistons: number;
  carType: CarTypeDTO;
}
