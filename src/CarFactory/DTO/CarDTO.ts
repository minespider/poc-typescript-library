import EngineDTO from "./EngineDTO";
import DoorDTO from "./DoorDTO";
import TrunkDoorDTO from "./TrunkDoorDTO";
import MultimediaDTO from "./MultimediaKitDTO";
import CarTypeDTO from "./CarTypeDTO";

export default interface CarDTO {
  id: string;
  name: string;
  color: string;
  manufacturer: string;
  carType: CarTypeDTO;
  engine: EngineDTO;
  frontDoorLeft?: DoorDTO;
  frontDoorRight?: DoorDTO;
  backDoorLeft?: DoorDTO;
  backDoorRight?: DoorDTO;
  trunkDoor?: TrunkDoorDTO;
  multimedia?: MultimediaDTO;
}
