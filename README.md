<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo">
    <img src="docs/images/logo.jpeg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Car Factory Library</h3>

  <p align="center">
    A conceptual Typescript Library implementation.
  </p>
</div>

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Describing the Domain](#describing-the-domain)
* [Describing the Approach](#describing-the-approach)
* [Usage](#usage)
* [Getting Started](#getting-started)
* [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Dependencies](#dependencies)
* [Contact](#contact)


## About the Project

This project was created as a reference to discuss the complete workflow for
creating a Typescript Library at **Minespider**.

This is a very tangible way of showing the implementation of the architecture.

### Built With

The project was built using:

* [TypeScript](https://www.typescriptlang.org/)


## Describing the Domain

The domain is a **Car Factory** to better approach the *ubiquitous* understanding
of the solution we are implementing.

**The goal:** build a *CarModel* stateful object to deliver it to the consumer.
Simple like that. 

We need to build a *library* to brings us a structured data of a *generic Car*.

One **Car** MUST have:

- name;
- manufacturer;
- color;
- engine specification;
- 1 front left door; 
- 1 front righ door;

One **Car** might have:

- 1 back left dor;
- 1 back right dor;
- 1 trunk door;
- multimedia kit;

There two possible data sources:

* Volkswagen API;
* Toyota Motors API;

Each data source may bring different data. They must be mapped and converted
to a common model interface to be used as standand by the current library.

### Describing the approach

The proposal fulfills the following approach:

<div align="center">
    <img src="docs/images/PoC-library-layers.png" alt="PoC Overview" width="800">
</div> 

_TO BE VALIDATED_

### Usage

After installing all the prerequisites softwares, let's install and use the library:

```sh
yarn install
```

After installation, you simply need to call the main object:

```typescript
import CarFactoryFacade from "CarFactoryLibrary";
const {getToyotaService, getVolkswagenService} = new CarFactoryFacade()

const volkswagenCars = getVolkswagenService().getAll(); // Collection of CarModel instances
const toyotaCars = getToyotaService().getAll(); // Collection of CarModel instances
```

## Getting Started

This is an example of how you may give instructions on setting up your project locally.

To get a local copy up and running follow these simple example steps.


### Prerequisites

Follow the links to find instruction of how to install the following softwares in your enviroment:

* [make](https://www.google.com/search?q=make+command+how+to+install&oq=make+command+how+to+install)
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Git flow](https://github.com/nvie/gitflow/wiki/Installation)
* [Yarn](https://classic.yarnpkg.com/en/)

**IMPORTANT:** make commando will vary according to your operating system.

### Installation

1. Clone the repo
```sh
$ git clone https://gitlab.com/minespider/poc-typescript-library
```

2. Install dependencies
```sh
$ yarn install
```

#### Dependencies

**Production dependencies:**

* [axios](https://www.npmjs.com/package/axios);

List all the dependencies. This will help to track how much the project is changing and its complexity.

**Development dependencies:**

* [jest](https://www.npmjs.com/package/jest)
* [@types/jest](https://www.npmjs.com/package/@types/jest);
* [ts-jest](https://www.npmjs.com/package/ts-jest)

<!-- CONTACT -->
## Contact

* Minespider Dev Team - email[@]example.com

Project Link: [https://gitlab.com/minespider/poc-typescript-library](https://gitlab.com/minespider/poc-typescript-library)