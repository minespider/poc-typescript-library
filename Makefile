unit-test:
	DOCKER_BUILDKIT=1 docker build --target development --tag $(REGISTRY)/$(IMAGE):localdev --ssh default --build-arg APP_VERSION=$(VERSION) .
	docker run --rm -it --init -v $(PWD):/app $(REGISTRY)/$(IMAGE):localdev jest

publish:
	npm login
	npm publish --access public